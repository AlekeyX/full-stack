$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    interval: 2000
  });

  $('#contacto').on('show.bs.modal', function (e) {
    console.log('se esta mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', true);

  });
  $('#contacto').on('shown.bs.modal', function (e) {
    console.log('se muestra');
  });
  $('#contacto').on('hide.bs.modal', function (e) {
    console.log('se oculta');
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
  });
  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('se oculto');
    $('#contactoBtn').prop('disabled', false);
  });
});
